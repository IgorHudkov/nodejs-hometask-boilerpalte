const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  if (res.err) {
    res.err.message.includes("not found") ? res.status(404) : res.status(400);
    res.send({ error: true, message: res.err.message });
  } else {
    next();
  }
};

exports.responseMiddleware = responseMiddleware;
