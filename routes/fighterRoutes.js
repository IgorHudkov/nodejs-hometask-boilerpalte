const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

        // GET /api/fighters
        // GET /api/fighters/:id
        // POST /api/fighters
        // PUT /api/fighters/:id
        // DELETE /api/fighters/:id


        router.get('/', (req, res) => {
            res.send(FighterService.getAllFighters());
        });
        
        router.get('/:id', (req, res) => {
            res.send(FighterService.search(req.query));
        });
        
        router.post('/', (req, res) => {
            res.send(FighterService.createNewFighter(req.body));
        });
        
        router.put('/:id', (req, res) => {
            const fighter = FighterService.search(req.query);
            res.send(FighterService.changeFighterData(fighter, req.body));
        })
        
        router.delete('/api/users/:id', (req, res) => {
            const fighter = FighterService.search(req.query); 
            res.send(FighterService.deleteFighter(fighter.id));
        })
        

module.exports = router;