const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
//         GET /api/users
//         GET /api/users/:id
//         POST /api/users
//         PUT /api/users/:id
//         DELETE /api/users/:id

router.use(responseMiddleware);

router.get('/', (req, res) => {
    const users = UserService.getAllUsers();
    res.send(users);
});

router.get('/:id', (req, res) => {
    res.send(UserService.search(req.query));
});

router.post('/', (req, res) => {
    res.send(UserService.createNewUser(req.body));
});

router.put('/:id', (req, res) => {
    const user = UserService.search(req.query);
    res.send(UserService.changeUserData(user, req.body));
})

router.delete('/api/users/:id', (req, res) => {
    const user = UserService.search(req.query);
    res.send(UserService.deleteUser(user.id));
})


module.exports = router;