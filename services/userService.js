const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAllUsers() {
        return UserRepository.getAll();
    }

    createNewUser(user) {
        return UserRepository.create(user);
    }

    changeUserData(user, data) {
        return UserRepository.update(user.id, data);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteUser(user) {
        return UserRepository.delete(user.id);
    }
}

module.exports = new UserService();