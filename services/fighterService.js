const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    
    getAllFighters() {
        return FighterRepository.getAll();
    }

    createNewFighter(fighter) {
        return FighterRepository.create(fighter);
    }

    changeFighterData(fighter, data) {
        return FighterRepository.update(fighter.id, data);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteFighter(fighter) {
        return FighterRepository.delete(fighter.id);
    }
}

module.exports = new FighterService();